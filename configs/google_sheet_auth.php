<?php

$client = new Google\Client();
$client->setApplicationName('Google Sheet API integration with php');
$client->setAuthConfig($config['credential']);
$client->setScopes([Google_Service_Sheets::SPREADSHEETS]);

$spreadsheetId = $config['spreadsheet_id'];
$getRange = $config['spreadsheet_range'];

$service = new Google_Service_Sheets($client);
$googleSheet = $service->spreadsheets_values->get($spreadsheetId, $getRange);
