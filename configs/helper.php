<?php

function view($file, $args) {
    extract($args);
    ob_start();
    require getcwd() . '/' . $file;
    $html = ob_get_contents();
    ob_get_clean();
    
    return $html;
}

function logger(string $message) {
    $logFilepath = getcwd() . '\logs';
    if (!is_dir($logFilepath)) mkdir($logFilepath);

    $logFilename = $logFilepath . '\log-' . date('Y-m-d') . '.txt';
    if (!file_exists($logFilename)) fopen($logFilename, 'w');
    
    file_put_contents($logFilename, $message . "\n");
}

function calculateTime(int $startTime, int $endTime) {
    $duration = $endTime - $startTime;
    $hours = (int) ($duration / 60 / 60);
    $minutes = (int) ($duration / 60) - $hours * 60;
    $seconds = (int) $duration - $hours * 60 * 60 - $minutes * 60;

    return ($hours == 0 ? "00":$hours) . ":" . ($minutes == 0 ? "00":($minutes < 10? "0".$minutes:$minutes)) . ":" . ($seconds == 0 ? "00":($seconds < 10? "0".$seconds:$seconds));

}