<?php

error_reporting(0);

require_once 'vendor/autoload.php';
require_once 'configs/app.php';
require_once 'configs/google_sheet_auth.php';
require_once 'configs/helper.php';


use Dompdf\Dompdf;
use App\ParsingSheet;


$proccessStartTime = microtime(true);

$parsingSheet = new ParsingSheet($googleSheet->getValues());
$groupKK = $parsingSheet->groupKK();

print '[INFO] Total KK : ' . $parsingSheet->getTotalKK() . " kepala keluarga\n";
print '[INFO] Total Kapitasi : ' . $parsingSheet->getTotalBody() . " kapitasi\n";

$timeEndProccessSheet = microtime(true);

print '[INFO] Time proccess Google Sheet elapsed : ' . calculateTime($proccessStartTime, $timeEndProccessSheet) . "\n\n";

$number = 1;
$promptCounter = 1;
$arrayTime = [];

foreach($groupKK as $kk) {
    $proccessPerKkStartTime = microtime(true);

    try {
        if ($promptCounter > 50) {
            $prompt = 'n';

            while(trim(strtolower($prompt)) === 'n') {
                echo "\n[Prompt] Data queue sudah mencapai 50. Lanjutkan ? [Y/n]: ";

                $handle = fopen("php://stdin", "r");
                $prompt = fgets($handle);

                fclose($handle);
                print "\n";
            }

            $promptCounter = 1;
        }

        $kepalaKk = $parsingSheet->getKepalaKK($kk);
        $alamat = $parsingSheet->getAlamat($kk);
        $noKk = $parsingSheet->getNoKk($kk);
        $tanggalMasuk = $parsingSheet->getTanggalMasuk($kk);

        $datetime = \DateTime::createFromFormat('d-m-Y', $tanggalMasuk);

        if (!$datetime) $datetime = date('Y-m-d');
        
        $datetime->modify('first day of next month');

        $tanggal = $datetime->format('d');
        $bulan = $namaBulan[$datetime->format('n')];
        $tahun = $datetime->format('Y');

        $createdAtSurat = "$tanggal $bulan $tahun";
        
        print $number . '. Create pdf file to "' . strtoupper($kepalaKk) . '"';
        
        $dompdf = new Dompdf();
        $dompdf->loadHtml(view('pdf_templates/pdf_templates.php', [
            'kepala_kk' => $kepalaKk,
            'alamat' => $alamat,
            'no_kk' => $noKk,
            'keluarga' => $kk,
            'createdAtSurat' => $createdAtSurat
        ]));

        $dompdf->setPaper('A4', 'potrait');
        $dompdf->render();

        $output = $dompdf->output();
        
        if (!is_dir('storages')) mkdir('storages');
        
        $filepath = __DIR__ . '\storages\\' . time() . $number . '-' . $kepalaKk. '.pdf';
        file_put_contents($filepath, $output);

        exec('PDFtoPrinter.exe "' . $filepath . '"');

        $entity = '&radic;';
        $squareRoot = '√';
        $squareRoot = html_entity_decode($entity);
        $squareRoot = mb_convert_encoding($entity, 'UTF-8', 'HTML-ENTITIES');

        $proccessPerKkEndTime = microtime(true);
        $proccessTime = calculateTime($proccessPerKkStartTime, $proccessPerKkEndTime);
        $arrayTime[] = $proccessTime;

        print ' | Time elapsed : ' . $proccessTime . ' | Status : OK ' . $squareRoot . "\r\n";

        $number++;
        $promptCounter++;

    } catch(\Exception $e) {
        logger($e->getMessage());
    }
}


print "\n\n";

$averageTime = date('H:i:s', array_sum(array_map('strtotime', $arrayTime)) / count($arrayTime));
print '[INFO] Average time elapsed per KK : ' . $averageTime . "\n";

$proccessEndTime = microtime(true);
print '[INFO] All time proccess elapsed : ' . calculateTime($proccessStartTime, $proccessEndTime) . "\n\n";

