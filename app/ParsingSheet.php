<?php

namespace App;

class ParsingSheet
{
    private $sheet;

    public function __construct($sheet) {
        $this->sheet = $sheet;
    }

    public function getHeader() {
        if (is_array($this->sheet) && count($this->sheet) > 0) {
            return $this->sheet[0];
        }

        return null;
    }

    public function getBody() {
        if (is_array($this->sheet) && count($this->sheet) > 1) {

            $row = [];

            foreach(array_slice($this->sheet, 1) as $data) {
                $row[] = $data;
            }

            return $row;
        }

        return null;
    }

    public function getTotalBody() {
        $body = $this->getBody();

        if (is_array($body)) return count($body);

        return 0;
    }

    public function getTotalKK() {
        $header = $this->getHeader();

        if (!$header) return 0;      

        $indexKK = array_search('NO KK', $header);

        $kk = unique_multidim_array($this->getBody(), $indexKK);

        return count($kk);
    }

    public function groupKK() {
        $kk = [];

        if (is_array($this->getBody()) && count($this->getBody()) > 0) {
            foreach($this->getBody() as $key => $value) {
                $kk[$value[8]][] = $value;
            }
        }

        return $kk;
    }

    public function getKepalaKK($kk) {
        try {
            if (is_array($kk)) {
                return $kk[0][7];
            }

            return null;

        } catch(\Exception $e) {
            return null;
        } 
    }

    public function getAlamat($kk) {
        try {
            $kepalaKK = $this->getKepalaKK($kk);
            if (!$kepalaKK) return null;

            $index = array_search($kepalaKK, $kk);
            
            return $kk[$index][13];

        } catch(\Exception $e) {
            return null;
        }
    }

    public function getNoKk($kk) {
        try {
            if (is_array($kk)) {
                return $kk[0][8];
            }

            return null;

        } catch(\Exception $e) {
            return null;
        } 
    }

    public function getTanggalMasuk($kk) {
        try {
            if (is_array($kk)) {
                return $kk[0][10];
            }

            return null;

        } catch(\Exception $e) {
            return null;
        } 
    }

    
}


function unique_multidim_array($array, $key) {
    $temp_array = array();
    $i = 0;
    $key_array = array();
   
    foreach($array as $val) {
        if (!in_array($val[$key], $key_array)) {
            $key_array[$i] = $val[$key];
            $temp_array[$i] = $val;
        }
        $i++;
    }
    
    return $temp_array;
}