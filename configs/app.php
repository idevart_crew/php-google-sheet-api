<?php

$config = [
    'credential'        => 'credentials.json', // Google sheet API credential
    'spreadsheet_id'    => '', // Google sheet ID (found at url google spread sheet)
    'spreadsheet_range' => '' // Google spread sheet range ex A1:P10
];

$namaBulan = [null, 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];